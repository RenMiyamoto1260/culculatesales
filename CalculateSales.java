package jp.alhinc.miyamoto_ren.calculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CalculateSales {
	public static void main(String[] args) {
		Map<String, String> branchNames = new HashMap<String, String>();
		Map<String, Long> branchSales = new HashMap<String, Long>();
		System.out.println("支店定義ファイルを開きます => " + args[0]);

		BufferedReader br = null;
		try {
			File file = new File(args[0], "branch.lst");
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {
				String[] branchInfo = line.split(",");
				branchNames.put(branchInfo[0], branchInfo[1]);
				branchSales.put(branchInfo[0], (long) 0);
			}
		} catch(IOException e) {
			System.out.println("エラーが発生しました。");
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}

		System.out.println("売上ファイルを開きます => " + args[0]);
		File dir = new File(args[0]); 						//output==> Users/renmiyamoto/Documents/ALH課題
		File[] files = dir.listFiles();
		List<File> fileLists = new ArrayList<File>();
		for(int i = 0; i < files.length; i++) { 			//output==> files.length = 7
			String file_name = files[i].getName(); 			//output==> 00000001.rcd...その他7ファイル) *順番はバラバラ
			if(file_name.matches("[0-9]{8}.rcd$")) {
				fileLists.add(files[i]);
			}
		}

		try {
			for(int i = 0; i < fileLists.size(); i++) {
				FileReader fr = new FileReader(fileLists.get(i));
				br = new BufferedReader(fr);

				String line;
				while((line = br.readLine()) != null) {
					String[] branchInfo = line.split("");
					long sales = Long.parseLong(branchInfo[1]);
					branchSales.put(branchInfo[0], sales);
					System.out.println(branchInfo);
				}
			}

		} catch(IOException e) {
			System.out.println("エラーが発生しました。");
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}
	}
}